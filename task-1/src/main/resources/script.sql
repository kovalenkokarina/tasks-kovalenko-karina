DROP TABLE IF EXISTS tariff;
DROP TABLE IF EXISTS user;
create table tariff(
	id int auto_increment not null,
    name varchar(255) not null,
    primary key(id)
);

create table user(
    id int auto_increment not null,
    name varchar(255) not null,
    balance decimal not null,
    tariff_id int not null,
    primary key (id),
    foreign key (tariff_id) references tariff(id)
);

insert into tariff (name) values ('Tariff 1');
insert into tariff (name) values ('Tariff 2');
insert into tariff (name) values ('Tariff 3');
insert into tariff (name) values ('Tariff 4');
insert into tariff (name) values ('Tariff 5');

insert into user (name, balance, tariff_id) values ('Karina',10,1);
insert into user (name, balance, tariff_id) values ('Dmitry',20,2);
insert into user (name, balance, tariff_id) values ('Zenya',30,3);
insert into user (name, balance, tariff_id) values ('Yana',40,4);
insert into user (name, balance, tariff_id) values ('Vika',50,5);