package com.karina.webUserDb.dao;

import com.karina.webUserDb.model.User;

public interface UserDao extends GenericDao<User> {
}
