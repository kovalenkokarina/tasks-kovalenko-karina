package com.karina.webUserDb.dao;

import java.util.List;

public interface GenericDao<T> {
    List<T> getAll();
}
