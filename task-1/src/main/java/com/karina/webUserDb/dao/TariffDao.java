package com.karina.webUserDb.dao;

import com.karina.webUserDb.model.Tariff;

import java.util.List;

public interface TariffDao extends GenericDao<Tariff> {

    Tariff getById(int id);

}
