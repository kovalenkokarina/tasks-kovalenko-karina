package com.karina.webUserDb.dao.jdbc;

import com.karina.webUserDb.dao.TariffDao;
import com.karina.webUserDb.model.Tariff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcTariffDao implements TariffDao {

    private static final String QUERY_SELECT = "SELECT * FROM tariff";
    private static final String QUERY_SELECT_BY_ID = "SELECT * FROM tariff WHERE id = ?";
    private static final String TARIFF_COLUMN_ID = "id";
    private static final String TARIFF_COLUMN_NAME = "name";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Tariff getById(int id) {
        return jdbcTemplate.queryForObject(QUERY_SELECT_BY_ID, new Object[]{id}, (resultSet,i) -> toTariff(resultSet));
    }

    @Override
    public List<Tariff> getAll() {
        return jdbcTemplate.query(QUERY_SELECT, (resultSet,i)-> toTariff(resultSet));
    }

    private Tariff toTariff(ResultSet resultSet) throws SQLException {
        return new Tariff(){{
           setId(resultSet.getInt(TARIFF_COLUMN_ID));
           setName(resultSet.getString(TARIFF_COLUMN_NAME));
        }};
    }
}
