package com.karina.webUserDb.dao.jdbc;

import com.karina.webUserDb.dao.TariffDao;
import com.karina.webUserDb.dao.UserDao;
import com.karina.webUserDb.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class JdbcUserDao implements UserDao{

    private static final String QUERY_SELECT = "SELECT * FROM user";
    private static final String USER_COLUMN_ID = "id";
    private static final String USER_COLUMN_NAME = "name";
    private static final String USER_COLUMN_BALANCE = "balance";
    private static final String USER_COLUMN_TARIFF = "tariff_id";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TariffDao tariffDao;

    @Override
    public List<User> getAll() {
        return jdbcTemplate.query(QUERY_SELECT, (resultSet,i)-> toUser(resultSet));
    }

    private User toUser(ResultSet resultSet) throws SQLException {
        return new User(){{
            setId(resultSet.getInt(USER_COLUMN_ID));
            setName(resultSet.getString(USER_COLUMN_NAME));
            setBalance(resultSet.getBigDecimal(USER_COLUMN_BALANCE));
            setTariff(tariffDao.getById(resultSet.getInt(USER_COLUMN_TARIFF)));
        }};
    }
}
