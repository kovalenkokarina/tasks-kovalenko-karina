package com.karina.webUserDb.controller;

import com.karina.webUserDb.exceptions.NoContentException;
import com.karina.webUserDb.model.User;
import com.karina.webUserDb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Configuration
@ComponentScan({"com.karina.webUserDb.service"})
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/users", method = RequestMethod.GET, produces= {MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<User> getAllUsers() {
        List<User> users = userService.getAll();
        if(users == null){
            throw new NoContentException();
        }
        return users;
    }


}
