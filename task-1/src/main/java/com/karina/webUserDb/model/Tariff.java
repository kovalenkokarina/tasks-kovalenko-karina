package com.karina.webUserDb.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(name = "tariff")
@Component
public class Tariff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;

    public Tariff(int id, String name){
        this.id = id;
        this.name = name;
    }

    public Tariff(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
