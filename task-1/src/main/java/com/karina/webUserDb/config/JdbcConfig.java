package com.karina.webUserDb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.karina.webUserDb")
public class JdbcConfig {

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
   public DataSource dataSource() {
              return new EmbeddedDatabaseBuilder()
                      .setType(EmbeddedDatabaseType.H2)
                      .setName("users")
                      .addScript("classpath:/script.sql")
                      .build();
         }
}
