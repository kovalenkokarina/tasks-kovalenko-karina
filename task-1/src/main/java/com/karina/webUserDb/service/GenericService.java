package com.karina.webUserDb.service;

import java.util.List;

public interface GenericService<T>{
    List<T> getAll();
}
