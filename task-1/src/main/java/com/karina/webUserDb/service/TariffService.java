package com.karina.webUserDb.service;

import com.karina.webUserDb.dao.TariffDao;
import com.karina.webUserDb.model.Tariff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffService implements GenericService<Tariff> {

    @Autowired
    private TariffDao tariffDao;

    @Override
    public List<Tariff> getAll() {
        return tariffDao.getAll();
    }
}
