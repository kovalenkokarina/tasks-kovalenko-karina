package com.karina.webUserDb.controller;

import com.karina.webUserDb.config.WebConfig;
import com.karina.webUserDb.model.Tariff;
import com.karina.webUserDb.model.User;
import com.karina.webUserDb.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.List;
import java.util.Arrays;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class})
public class UserControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .build();
    }

    @Test
    public void testJsonGetAllSuccess() throws Exception {
        List<User> users = Arrays.asList(
                new User(1,"user1", new BigDecimal(30), new Tariff(1, "tariff1")),
                new User(2,"user2", new BigDecimal(40), new Tariff(2, "tariff2")));

        when(userService.getAll()).thenReturn(users);

        mockMvc.perform(get("/users.json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("user1")))
                .andExpect(jsonPath("$[0].balance", is(30)))
                .andExpect(jsonPath("$[0].tariff.id", is(1)))
                .andExpect(jsonPath("$[0].tariff.name", is("tariff1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("user2")))
                .andExpect(jsonPath("$[1].balance", is(40)))
                .andExpect(jsonPath("$[1].tariff.id", is(2)))
                .andExpect(jsonPath("$[1].tariff.name", is("tariff2")));

        verify(userService, times(1)).getAll();
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void testXMLGetAllSuccess() throws Exception {
        List<User> users = Arrays.asList(
                new User(1,"user1", new BigDecimal(30), new Tariff(1, "tariff1")),
                new User(2,"user2", new BigDecimal(40), new Tariff(2, "tariff2"))
        );

        when(userService.getAll()).thenReturn(users);

        mockMvc.perform(get("/users.xml"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML))
                .andExpect(xpath("ArrayList/item[1]").exists())
                .andExpect(xpath("ArrayList/item[1]/*").nodeCount(is(4)))
                .andExpect(xpath("ArrayList/item[1]/id").exists())
                .andExpect(xpath("ArrayList/item[1]/id").string(is("1")))
                .andExpect(xpath("ArrayList/item[1]/balance").exists())
                .andExpect(xpath("ArrayList/item[1]/balance").string(is("30")))
                .andExpect(xpath("ArrayList/item[1]/name").exists())
                .andExpect(xpath("ArrayList/item[1]/name").string(is("user1")))
                .andExpect(xpath("ArrayList/item[1]/tariff/id").exists())
                .andExpect(xpath("ArrayList/item[1]/tariff/id").string(is("1")))
                .andExpect(xpath("ArrayList/item[1]/tariff/name").exists())
                .andExpect(xpath("ArrayList/item[1]/tariff/name").string(is("tariff1")))
                .andExpect(xpath("ArrayList/item[2]").exists())
                .andExpect(xpath("ArrayList/item[2]/*").nodeCount(is(4)))
                .andExpect(xpath("ArrayList/item[2]/id").exists())
                .andExpect(xpath("ArrayList/item[2]/id").string(is("2")))
                .andExpect(xpath("ArrayList/item[2]/balance").exists())
                .andExpect(xpath("ArrayList/item[2]/balance").string(is("40")))
                .andExpect(xpath("ArrayList/item[2]/name").exists())
                .andExpect(xpath("ArrayList/item[2]/name").string(is("user2")))
                .andExpect(xpath("ArrayList/item[2]/tariff/id").exists())
                .andExpect(xpath("ArrayList/item[2]/tariff/id").string(is("2")))
                .andExpect(xpath("ArrayList/item[2]/tariff/name").exists())
                .andExpect(xpath("ArrayList/item[2]/tariff/name").string(is("tariff2")));

        verify(userService, times(1)).getAll();
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void testGetAllFailNoContent() throws Exception {
        when(userService.getAll()).thenReturn(null);

        mockMvc.perform(get("/users", 1))
                .andExpect(status().isNoContent());

        verify(userService, times(1)).getAll();
        verifyNoMoreInteractions(userService);
    }

}