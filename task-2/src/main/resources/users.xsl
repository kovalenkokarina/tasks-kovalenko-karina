<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <clients>
            <xsl:for-each select="ArrayList/item">
                <client>
            <client_id>
                    <xsl:value-of select="id"></xsl:value-of>
            </client_id>
            <balance>
                <xsl:value-of select="balance"></xsl:value-of>
            </balance>
                </client>
            </xsl:for-each>
        </clients>
    </xsl:template>

</xsl:stylesheet>