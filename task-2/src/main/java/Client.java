import java.math.BigDecimal;

public class Client {

    private int client_id;

    private BigDecimal balance;

    public Client(int id, BigDecimal balance){
        this.client_id = id;
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getId() {
        return client_id;
    }

    public void setId(int client_id) {
        this.client_id = client_id;
    }

}
