import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class SaveToCache {
    private final String USERNAME = "Administrator";
    private final String PASSWORD = "password";
    private String BUCKET_NAME;
    private String HOST_COUCHBASE;
    private String httpUrl;
    private URL url;
    private HttpURLConnection connection;
    private CouchbaseCluster cluster;

    public SaveToCache(String portFirstApp, String cacheHost, String bucketName) throws IOException {
        HOST_COUCHBASE = cacheHost;
        BUCKET_NAME = bucketName;
        httpUrl = "http://localhost:"+portFirstApp+"/users.xml";
        url = new URL(httpUrl);
        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
                .queryTimeout(10000000)
                .kvTimeout(10000000)
                .searchTimeout(1000000)
                .socketConnectTimeout(1000000000)
                .managementTimeout(1000000000)
                .connectTimeout(10000000).build();
        cluster = CouchbaseCluster.create(env, HOST_COUCHBASE);
        cluster.authenticate(USERNAME, PASSWORD);
    }

    private void saveToCache(List<Client> clients){
        JsonArray jsonArray = JsonArray.empty();
        JsonObject array = JsonObject.create();
        clients.forEach(c->{
            JsonObject content = JsonObject.create()
                    .put("client_id", c.getId())
                    .put("balance", c.getBalance());
            jsonArray.add(content);
            array.put("users", jsonArray);
        });
        Bucket bucket = cluster.openBucket(BUCKET_NAME);
        bucket.bucketManager().createN1qlPrimaryIndex(true , false );
        JsonDocument jsonDocument = bucket.upsert(JsonDocument.create("clientBalanceDoc", array));
        System.out.println(jsonDocument);
    }

    public void getUsersAndSaveToCache() throws IOException {
        connection = (HttpURLConnection)url.openConnection();
        try(BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))){String input;
            StringBuilder stringBuilder = new StringBuilder();
            while ((input = br.readLine()) != null){
                stringBuilder.append(input);
            }
            saveToCache(XmlConverter.convertXmlUsingXSLT(stringBuilder.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
