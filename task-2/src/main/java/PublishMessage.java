import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class PublishMessage {

    private CouchbaseCluster cluster;
    private ConnectionFactory factory;
    private final String USERNAME_PASSWORD_RABBITMQ = "rabbitmq";
    private String USERNAME_COUCHBASE;
    private String PASSWORD_COUCHBASE;
    private String BUCKET_NAME;
    private final String QUERY = "SELECT u.* FROM `clientBalanceBucket` m UNNEST m.users as u where u.balance < 30;";
    private String HOST_COUCHBASE;
    private final String QUEUE_NAME = "clientBalanceQueue";

    public PublishMessage(String cacheUsername, String cachePassword, String cacheHost, String bucketName){
        USERNAME_COUCHBASE = cacheUsername;
        PASSWORD_COUCHBASE = cachePassword;
        BUCKET_NAME = bucketName;
        HOST_COUCHBASE = cacheHost;
        factory = new ConnectionFactory();
        factory.setPassword(USERNAME_PASSWORD_RABBITMQ);
        factory.setUsername(USERNAME_PASSWORD_RABBITMQ);
        factory.setHost("localhost");
        cluster = CouchbaseCluster.create(HOST_COUCHBASE);
        cluster.authenticate(USERNAME_COUCHBASE, PASSWORD_COUCHBASE);
    }

    public void sendToRabbitMQ(){
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = getMessageFromCache();
            if(!message.equals("")) {
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
                System.out.println("[x] Sent '" + message + "'");
            }
        } catch (TimeoutException| IOException e) {
            e.printStackTrace();
        }
    }

    private String getMessageFromCache(){
        Bucket bucket = cluster.openBucket(BUCKET_NAME);
        N1qlQueryResult result = bucket.query(N1qlQuery.simple(QUERY));
        if(result.finalSuccess()) {
            return result.allRows().toString();
        }
        return "";
    }
}
