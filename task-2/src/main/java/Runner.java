import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Runner {
    public static void main(String[]args) throws IOException {

        String usernameCouchbase = args[0];
        String passwordCouchbase = args[1];
        String bucketName = args[2];
        String hostCouchbase = args[3];
        String portFirstApp = args[4];

        SaveToCache saveToCache = new SaveToCache(portFirstApp,hostCouchbase,bucketName);
        PublishMessage publishMessage = new PublishMessage(usernameCouchbase,passwordCouchbase,hostCouchbase,bucketName);

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);

        Runnable saveCacheTask = () ->{
            try {
                saveToCache.getUsersAndSaveToCache();
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        Runnable publishTask = publishMessage::sendToRabbitMQ;

        scheduledExecutorService.scheduleAtFixedRate(saveCacheTask, 0,10, TimeUnit.SECONDS);
        scheduledExecutorService.scheduleAtFixedRate(publishTask, 5,10, TimeUnit.SECONDS);
    }
}
