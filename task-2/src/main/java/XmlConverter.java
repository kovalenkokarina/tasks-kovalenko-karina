import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class XmlConverter {

    public static List<Client> convertXmlUsingXSLT(String xml) {
        List<Client> clients = null;
        try {
            StringWriter writer = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer(new StreamSource(XmlConverter.class.getResource("users.xsl").toString()));
            transformer.transform(new StreamSource(new StringReader(xml)), new StreamResult(writer));
            clients = unmarshalling(writer.toString());
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return clients;
    }

    private static List<Client> unmarshalling(String xml){
        XStream xStream = new XStream(new DomDriver());
        xStream.alias("clients",List.class);
        xStream.alias("client",Client.class);
        xStream.aliasField("client_id", Client.class, "client_id");
        xStream.aliasField("balance", Client.class, "balance");
        return (ArrayList<Client>) xStream.fromXML(xml);
    }
}
