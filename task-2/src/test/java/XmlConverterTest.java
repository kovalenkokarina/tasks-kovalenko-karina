import com.couchbase.client.java.cluster.User;
import com.thoughtworks.xstream.converters.ConversionException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class XmlConverterTest {

    @Test
    public void testConvertXmlUsingXSLTAndGetListSuccess() {
        List<Client> clientsExpected = Arrays.asList(
                new Client(1, new BigDecimal(10)),
                new Client(2, new BigDecimal(20)));
        String xml = "<ArrayList>\n" +
                "<item>\n" +
                "<id>1</id>\n" +
                "<name>Karina</name>\n" +
                "<balance>10</balance>\n" +
                "<tariff>\n" +
                "<id>1</id>\n" +
                "<name>Tariff 1</name>\n" +
                "</tariff>\n" +
                "</item>\n" +
                "<item>\n" +
                "<id>2</id>\n" +
                "<name>Dmitry</name>\n" +
                "<balance>20</balance>\n" +
                "<tariff>\n" +
                "<id>2</id>\n" +
                "<name>Tariff 2</name>\n" +
                "</tariff>\n" +
                "</item>" +
                "</ArrayList>";
        List<Client> clientsActual = XmlConverter.convertXmlUsingXSLT(xml);
        Assert.assertEquals(clientsExpected.get(0).getId(), clientsActual.get(0).getId());
        Assert.assertEquals(clientsExpected.get(0).getBalance(), clientsActual.get(0).getBalance());
        Assert.assertEquals(clientsExpected.get(1).getId(), clientsActual.get(1).getId());
        Assert.assertEquals(clientsExpected.get(1).getBalance(), clientsActual.get(1).getBalance());
    }

    @Test(expected = ConversionException.class)
    public void testConvertXmlUsingXSLTAndGetListFailConversionException() {
        List<Client> clientsExpected = Arrays.asList(
                new Client(1, new BigDecimal(10)),
                new Client(2, new BigDecimal(20)));
        String xml = "<ArrayList>\n" +
                "<item>\n" +
                "<id>1</id>\n" +
                "<name>Karina</name>\n" +
                "<balance>10</balance>\n" +
                "<tariff>\n" +
                "<id>1</id>\n" +
                "<name>Tariff 1</name>\n" +
                "</tariff>\n" +
                "</item>\n" +
                "<item>\n" +
                "<name>Dmitry</name>\n" +
                "<balance>20</balance>\n" +
                "<tariff>\n" +
                "<name>Tariff 2</name>\n" +
                "</tariff>\n" +
                "</item>" +
                "</ArrayList>";
        List<Client> clientsActual = XmlConverter.convertXmlUsingXSLT(xml);

        Assert.assertEquals(clientsExpected.get(0).getId(), clientsActual.get(0).getId());
        Assert.assertEquals(clientsExpected.get(0).getBalance(), clientsActual.get(0).getBalance());
        Assert.assertEquals(clientsExpected.get(1).getId(), clientsActual.get(1).getId());
        Assert.assertEquals(clientsExpected.get(1).getBalance(), clientsActual.get(1).getBalance());
    }
}
